package com.asset.management.AssetManagement.exceptions;

public class AssetAssignmentException extends RuntimeException {
    public AssetAssignmentException(String message) {
        super(message);
    }
}
