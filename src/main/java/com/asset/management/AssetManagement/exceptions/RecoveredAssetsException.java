package com.asset.management.AssetManagement.exceptions;

public class RecoveredAssetsException extends RuntimeException {
    public RecoveredAssetsException(String message) {
        super(message);
    }
}
