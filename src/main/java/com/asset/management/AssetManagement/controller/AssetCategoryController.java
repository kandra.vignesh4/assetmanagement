package com.asset.management.AssetManagement.controller;

import com.asset.management.AssetManagement.entities.Asset;
import com.asset.management.AssetManagement.entities.AssetCategory;
import com.asset.management.AssetManagement.service.AssetCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/asset-categories")
public class AssetCategoryController {
    @Autowired
    AssetCategoryService assetCategoryService;

    @GetMapping("/get-categories")
    public ResponseEntity<List<AssetCategory>> getAllCategories (){
        List<AssetCategory> response = assetCategoryService.getAllCategories();
        return ResponseEntity.ok(response);
    }

    @PostMapping("/add-category")
    public void addAsset (@RequestBody AssetCategory assetCategory) {
        assetCategoryService.addAssetCategory(assetCategory);
    }
}
