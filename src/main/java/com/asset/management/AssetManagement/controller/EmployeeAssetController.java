package com.asset.management.AssetManagement.controller;

import com.asset.management.AssetManagement.entities.Asset;
import com.asset.management.AssetManagement.entities.Employee;
import com.asset.management.AssetManagement.service.EmployeeAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee-assets")
public class EmployeeAssetController {
    @Autowired
    EmployeeAssetService employeeAssetService;

    @PutMapping("/assets/assign")
    public void assignAssetToEmployee(@RequestBody Asset asset, @RequestBody Employee employee) {
        employeeAssetService.assignAsset(asset, employee);
    }

    @GetMapping("/assets/recovered")
    public ResponseEntity<List<Asset>> getRecoveredAssets() {
        return ResponseEntity.ok(employeeAssetService.recoveredAssets());
    }
}
