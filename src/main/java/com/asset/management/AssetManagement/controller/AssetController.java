package com.asset.management.AssetManagement.controller;

import com.asset.management.AssetManagement.entities.Asset;
import com.asset.management.AssetManagement.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/assets")
public class AssetController {
    @Autowired
    AssetService assetService;

    @GetMapping("/get-assets")
    public ResponseEntity<List<Asset>> getAllAssets(){
        return ResponseEntity.ok(assetService.getAllAssets());
    }

    @GetMapping("/get-assets/asset")
    public ResponseEntity<Asset> getAssetByname (@RequestParam String assetName) {
        return ResponseEntity.ok(assetService.getAssetByName(assetName));
    }

    @PostMapping("/get-assets/asset/add")
    public void addAsset (@RequestBody Asset asset) {
        assetService.addAsset(asset);
    }

    @PutMapping("/get-assets/asset/update")
    public void updateAsset (@RequestBody Asset asset) {
        assetService.updateAsset(asset);
    }

    @DeleteMapping("/get-assets/asset/delete")
    public void deleteAsset (@RequestBody Asset asset) {
        assetService.deleteAsset(asset);
    }
}
