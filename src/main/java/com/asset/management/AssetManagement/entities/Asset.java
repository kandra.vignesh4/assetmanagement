package com.asset.management.AssetManagement.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Asset {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int assetId;
    private String name;
    private Date purchaseDate;
    private String conditionNotes;

    @Enumerated(EnumType.STRING)
    private AssignmentStatus assignmentStatus;

    @OneToOne
    @JoinColumn(name = "categoryId")
    private AssetCategory assetCategory;

}