package com.asset.management.AssetManagement.entities;

public enum AssignmentStatus {
    Available, Assigned, Recovered;
}
