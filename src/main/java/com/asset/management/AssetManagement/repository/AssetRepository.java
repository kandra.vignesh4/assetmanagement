package com.asset.management.AssetManagement.repository;

import com.asset.management.AssetManagement.entities.Asset;
import com.asset.management.AssetManagement.entities.AssetCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssetRepository extends JpaRepository<Asset, Integer> {
    @Query("SELECT a FROM Asset a WHERE a.name = :name")
    public Asset findByNameAndAssociatedMethod(@Param("name") String name);

    @Query("SELECT a FROM Asset a WHERE a.assignmentStatus = 'Recovered'")
    public List<Asset> findByAssignmentStatusRecovered();

}
