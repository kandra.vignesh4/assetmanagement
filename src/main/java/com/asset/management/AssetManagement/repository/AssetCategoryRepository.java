package com.asset.management.AssetManagement.repository;

import com.asset.management.AssetManagement.entities.AssetCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssetCategoryRepository extends JpaRepository<AssetCategory, Integer> {
}
