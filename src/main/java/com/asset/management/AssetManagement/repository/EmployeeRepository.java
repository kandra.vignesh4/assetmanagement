package com.asset.management.AssetManagement.repository;

import com.asset.management.AssetManagement.entities.AssetCategory;
import com.asset.management.AssetManagement.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
