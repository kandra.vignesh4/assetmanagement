package com.asset.management.AssetManagement.service;

import com.asset.management.AssetManagement.entities.Asset;

import java.util.List;

public interface AssetService {
    public List<Asset> getAllAssets();
    public void addAsset(Asset asset);
    public Asset getAssetByName(String assetName);
    public void updateAsset(Asset asset);
    public void deleteAsset(Asset asset);
}
