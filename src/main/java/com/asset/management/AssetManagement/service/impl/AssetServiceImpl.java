package com.asset.management.AssetManagement.service.impl;

import com.asset.management.AssetManagement.entities.Asset;
import com.asset.management.AssetManagement.repository.AssetRepository;
import com.asset.management.AssetManagement.service.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssetServiceImpl implements AssetService {
    @Autowired
    AssetRepository assetRepository;

    @Override
    public List<Asset> getAllAssets() {
        return assetRepository.findAll();
    }

    @Override
    public void addAsset(Asset asset) {
        assetRepository.save(asset);
    }

    @Override
    public Asset getAssetByName(String assetName) {
        return assetRepository.findByNameAndAssociatedMethod(assetName) ;
    }

    @Override
    public void updateAsset(Asset asset) {
        assetRepository.save(asset);
    }

    @Override
    public void deleteAsset(Asset asset) {
        assetRepository.delete(asset);
    }
}
