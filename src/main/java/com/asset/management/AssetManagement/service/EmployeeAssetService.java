package com.asset.management.AssetManagement.service;

import com.asset.management.AssetManagement.entities.Asset;
import com.asset.management.AssetManagement.entities.Employee;

import java.util.List;

public interface EmployeeAssetService {
    public void assignAsset(Asset asset, Employee employee);
    public List<Asset> recoveredAssets();
}
