package com.asset.management.AssetManagement.service.impl;

import com.asset.management.AssetManagement.entities.AssetCategory;
import com.asset.management.AssetManagement.repository.AssetCategoryRepository;
import com.asset.management.AssetManagement.service.AssetCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssetCategoryServiceImpl implements AssetCategoryService {
    @Autowired
    AssetCategoryRepository assetCategoryRepository;

    @Override
    public List<AssetCategory> getAllCategories() {
        return assetCategoryRepository.findAll();
    }

    @Override
    public void addAssetCategory(AssetCategory assetCategory) {
        assetCategoryRepository.save(assetCategory);
    }

    @Override
    public void updateAssetCategory(AssetCategory assetCategory) {
        assetCategoryRepository.save(assetCategory);
    }
}
