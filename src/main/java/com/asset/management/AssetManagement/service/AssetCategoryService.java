package com.asset.management.AssetManagement.service;

import com.asset.management.AssetManagement.entities.AssetCategory;

import java.util.List;

public interface AssetCategoryService {
    public List<AssetCategory> getAllCategories();

    void addAssetCategory(AssetCategory assetCategory);

    public void updateAssetCategory(AssetCategory assetCategory);
}
