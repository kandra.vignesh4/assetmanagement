package com.asset.management.AssetManagement.service.impl;

import com.asset.management.AssetManagement.entities.Asset;
import com.asset.management.AssetManagement.entities.Employee;
import com.asset.management.AssetManagement.repository.AssetRepository;
import com.asset.management.AssetManagement.repository.EmployeeRepository;
import com.asset.management.AssetManagement.service.EmployeeAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeAssetServiceImpl implements EmployeeAssetService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    AssetRepository assetRepository;

    @Override
    public void assignAsset(Asset asset, Employee employee) {
        employee.getAssets().add(asset);
        employeeRepository.save(employee);
    }

    @Override
    public List<Asset> recoveredAssets() {
        return assetRepository.findByAssignmentStatusRecovered();
    }
}
