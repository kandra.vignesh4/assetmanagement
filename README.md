# Asset Management Application

Welcome to the Asset Management application! This Spring Boot project manages assets using an H2 in-memory database.

## Prerequisites

Before you begin, ensure you have the following installed on your system:

- Java Development Kit (JDK) 17 or higher: [Download JDK](https://www.oracle.com/java/technologies/javase-jdk17-downloads.html)
- Maven: [Download Maven](https://maven.apache.org/download.cgi)
- Git: [Download Git](https://git-scm.com/downloads)

## Getting Started

To get a local copy of this project and run it on your machine, follow these steps:

1. Clone the repository to your local machine:
   git clone https://gitlab.com/kandra.vignesh4/assetmanagement.git

cd asset-management

mvn clean install

Running the Application
Once the project is successfully built, you can run the application locally. Follow these steps:

Navigate to the project directory if you're not already there:

 
cd asset-management
Run the application using the Maven Spring Boot plugin:

 
mvn spring-boot:run
Once the application is running, you can access the Swagger UI at the following URL:

 
http://localhost:8007/asset-management/swagger-ui/index.html
This provides a user-friendly interface to interact with the application's APIs.

H2 Database Console
The H2 in-memory database console is enabled in this application. You can access it at the following URL:

 
http://localhost:8007/asset-management/h2-console
When prompted for database connection details, use the following:

JDBC URL: jdbc:h2:mem:assetmanagementdb
Username: sa
Password: password
Additional Information
The application runs on port 8007, and the context path is /asset-management.
Make sure no other application is running on the same port to avoid conflicts.
